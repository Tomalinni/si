import {Injectable, OnInit} from '@angular/core';
import {Headers, Http, RequestOptions, RequestOptionsArgs, ResponseContentType} from '@angular/http';
import {ApiConnectService} from './api-connect.service';
import * as moment from 'moment';
import {TokenService} from './token.service';
import {Principal} from '../models/principal.model';
import {Observable} from 'rxjs/Observable';
import {SelfUserInfo} from '../models/self-user-info.model';


@Injectable()
export class UserService implements OnInit {
  private timeUTC;

  constructor(private http: Http,
              private apiURL: ApiConnectService,
              private tokenService: TokenService) {
  }

  ngOnInit(): void {
  }

  getPrincipal(): Observable<any> {
    console.log(this.tokenService.getHeaders());
    return this.http.get(this.apiURL.getSessionUser, {headers: this.tokenService.getHeaders()});
  }

  getTimezoneOffset() {
    if (moment().utcOffset() >= 0) {
      if (moment().utcOffset() / 60 < 10) {
        // return '+0' + moment().utcOffset() / 60 + ':00';
        this.timeUTC = '+0' + moment().utcOffset() / 60 + ':00';
        // console.log(this.timeUTC);
      } else {
        // return '+' + moment().utcOffset() / 60 + ':00';
        this.timeUTC = '+' + moment().utcOffset() / 60 + ':00';
        // console.log(this.timeUTC);
      }
    } else {
      if (moment().utcOffset() / 60 > -10) {
        // return '-0' + (moment().utcOffset() / 60).toString().split('-')[1] + ':00';
        this.timeUTC = '-0' + (moment().utcOffset() / 60).toString().split('-')[1] + ':00';
        // console.log(this.timeUTC);
      } else {
        // return moment().utcOffset() / 60 + ':00';
        this.timeUTC = moment().utcOffset() / 60 + ':00';
        // console.log(this.timeUTC);
      }
    }
    // return this.timeUTC;
  }

  public getSelfUserInfo(): any {
    return this.http.get(this.apiURL.getSelfInfo, {headers: this.tokenService.getHeaders()});
  }

  public createUser(user) {
    return this.http.post(this.apiURL.createUser, JSON.stringify(user), { headers: this.tokenService.getHeaders() });
  }
}
