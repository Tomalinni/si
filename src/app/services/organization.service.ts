import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import { ApiConnectService } from './api-connect.service';

import {OrganizationModel} from '../models/organization.model';
import {Observable} from 'rxjs/Observable';
import {TokenService} from './token.service';
import { Response } from '@angular/http/src/static_response';
import {AdminModel} from "../models/admin.model";

@Injectable()
export class OrganizationService {

  // *** Here all the functionality of the administrator ***;

  // constructor(private http: Http,
  //             private apiURL: ApiConnectService) {
  // }

  private url = 'http://localhost:64269/api/users/';
  constructor(private http: Http,
              private apiURL: ApiConnectService,
              private tokenService: TokenService) { }

  getOrganizations(): Observable<OrganizationModel[]> {
    return this.http.get(this.apiURL.getAllOrganizations, {headers: this.tokenService.getHeaders()}).map((resp:Response) => {
      let data = resp.json();
      let respParse = data["_embedded"].organization;
      let organizations: OrganizationModel[] = [];
      for(let organization of respParse) {
        let links = organization._links;
        organizations.push({
          name: organization.name,
          address: organization.address,
          admins: links.admins.href,
          organizations: links.organization.href,
          self: links.self.href,
          users: links.users.href
        })
      };
      return organizations;
    })
  }

  getOrganizationAdmins(url) {
    return this.http.get(url, {headers: this.tokenService.getHeaders()}).map((resp: Response) => {
      console.log(resp.json()); // TODO this method
    });
  }

  createOrganization(organization: OrganizationModel) {
    let dataForRegister = {
      name: organization.name,
      address: organization.address
    }
    return this.http.post(this.apiURL.createCompany, JSON.stringify(dataForRegister), { headers: this.tokenService.getHeaders() });
  }
  /*
  updateOrganization(id: number, organization: OrganizationModel) {
    const urlParams = new HttpParams().set('id', id.toString());
    return this.http.put(this.url, organization, { params: urlParams});
  }
  deleteOrganization(id: number) {
    const urlParams = new HttpParams().set('id', id.toString());
    return this.http.delete(this.url, { params: urlParams});
  }*/

}
