import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FileUtil } from './file.util';
import { Constants } from './test.constants';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})



export class UserManagementComponent implements OnInit {

  @ViewChild('fileImportInput')
  fileImportInput: any;

  csvRecords = [];
  csvChecked = [];
  csvRecordsTemp = [];



  constructor(private _router: Router,
              private _fileUtil: FileUtil
  ) { }


  ngOnInit() {
  }


  delItem(): void {
    console.log('in method');
    for (const n of this.csvChecked) {
      console.log('in for');
      if (this.csvRecords.includes(n))
        console.log('in if');
        const ind: number = this.csvRecords.indexOf(n);
        if (ind !== -1) {
          this.csvRecords.splice(ind, 1);
        }
      }
    this.csvChecked = [];
  }

  fieldsChangePos(values: any, csvRec) {
    if (values.currentTarget.checked) {
      if (!this.csvChecked.includes(csvRec)) {
        this.csvChecked.push(csvRec);
      }
    } else {
      if (this.csvChecked.includes(csvRec)) {
        const ind: number = this.csvChecked.indexOf(csvRec);
        if (ind !== -1) {
          this.csvChecked.splice(ind, 1);
        }
      }
    }
    console.log(this.csvChecked);
  }



  // METHOD CALLED WHEN CSV FILE IS IMPORTED
  fileChangeListener($event): void {

    const text = [];
    const target = $event.target || $event.srcElement;
    const files = target.files;

    if (Constants.validateHeaderAndRecordLengthFlag){
      if (!this._fileUtil.isCSVFile(files[0])){
        alert('Please import valid .csv file.');
        this.fileReset();
      }
    }

    const input = $event.target;
    const reader = new FileReader();
    reader.readAsText(input.files[0]);

    reader.onload = (data) => {
      const csvData = reader.result;
      const csvRecordsArray = csvData.split(/\r\n|\n/);

      let headerLength = -1;
      if (Constants.isHeaderPresentFlag){
        const headersRow = this._fileUtil.getHeaderArray(csvRecordsArray, Constants.tokenDelimeter);
        headerLength = headersRow.length;
      }

      // this.csvRecords.push(this._fileUtil.getDataRecordsArrayFromCSVFile(csvRecordsArray,

      // this.csvRecords = this._fileUtil.getDataRecordsArrayFromCSVFile(csvRecordsArray,
      //   headerLength, Constants.validateHeaderAndRecordLengthFlag, Constants.tokenDelimeter);
      // console.log(this.csvRecords);

      this.csvRecordsTemp = this._fileUtil.getDataRecordsArrayFromCSVFile(csvRecordsArray,
        headerLength, Constants.validateHeaderAndRecordLengthFlag, Constants.tokenDelimeter);
      console.log(this.csvRecords);

      for (let i = 0; i < this.csvRecordsTemp.length; i++) {
        this.csvRecords.push(this.csvRecordsTemp[i]);
      }

      this.csvRecordsTemp = [];

      if (this.csvRecords == null) {  // If control reached here it means csv file contains error, reset file.
        // this.fileReset();
      }
    };

    reader.onerror = function () {
      alert('Unable to read ' + input.files[0]);
    };
  }

  fileReset() {
    this.fileImportInput.nativeElement.value = '';
    this.csvRecords = [];
  }
}
