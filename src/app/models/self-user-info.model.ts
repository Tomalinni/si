import {Role} from './role.model';

export class SelfUserInfo {
  public id: number;
  public email: string;
  public username: string;
  public first: string;
  public last: string;
  public roles: Role[];

  private enabled: boolean;
  private activated: boolean;
  private tokenFCM: string;
}
